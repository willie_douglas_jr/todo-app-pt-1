import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from "uuid";

class App extends Component {
  state = {
    todos: todosList,
    inputValue: ""
  };

  

  handleAddTodo = (event) => {
    if (event.key === 'Enter') {
    console.log(event.key);
    let new2Do = {
      userId: 1,
      id: uuidv4(),
      title: this.state.inputValue,
      completed: false,
    };
    let brandNewToDos = this.state.todos.slice()
    brandNewToDos.push(new2Do)
    this.setState({todos: brandNewToDos, inputValue: " "})
  }};


  handleChanges = (event) => {
    this.setState({inputValue: event.target.value})
    
    console.log(this.state.inputValue);
  };

  handleCompleted = (id) => {
   let veryNewTodo = this.state.todos.map((todo) => {
      // todo.id === id ? todo.completed = !todo.completed :
      console.log(id)
      if (todo.id === id) {
       todo.completed = !todo.completed
      } 
      return todo
   })
   this.setState({todos: veryNewTodo})
    console.log(this.state.todos)
  }

  handleDelete = (id) => {
    let newestTodo = this.state.todos.filter((todo) => {
      if (todo.id === id) {
        return false
      } else {
        return true
      }
    }) 
    this.setState({todos: newestTodo})
    }

  handleDeleteAll = () => {
    let newestTodo = this.state.todos.filter((todo) => {
      if (todo.completed === true) {
        return false
      } else {
        return true
      }
    }) 
    this.setState({todos: newestTodo})
    }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            onKeyDown={this.handleAddTodo}
            onChange={this.handleChanges}
            value={this.state.inputValue}
          />
        </header>
        <TodoList todos={this.state.todos} handleCompleted={this.handleCompleted} handleDelete={this.handleDelete} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleDeleteAll}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={() => this.props.handleCompleted(this.props.id)}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={() => this.props.handleDelete(this.props.id)} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem title={todo.title} completed={todo.completed} handleCompleted={this.props.handleCompleted} id={todo.id} key={todo.id} handleDelete={this.props.handleDelete} />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
